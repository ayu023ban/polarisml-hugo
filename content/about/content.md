+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Polaris"
#subtitle = ""
+++

Polaris is an open source project that analyzes satellite telemetry using machine learning.  It is a project of the Libre Space Foundation.

Want to get started quickly? Have a look at [Getting started with Polaris](https://docs.polarisml.space/en/latest/using/getting_started_with_polaris.html).
---
